const express = require("express");
const router = express.Router();
const userCtrl = require("../controllers/user");
const auth = require("../middleware/auth");

router.get("/users", userCtrl.list);
router.get("/users/:id", userCtrl.details);
router.post("/users/auth", userCtrl.auth);
router.post("/users", userCtrl.store);
router.put("/users/:id", auth, userCtrl.update);
router.delete("/users/:id", auth, userCtrl.remove);

module.exports = router;
