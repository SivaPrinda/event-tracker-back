const mongoose = require("mongoose");
const { Schema, model } = mongoose;
const uniqueValidator = require("mongoose-unique-validator");

const UserSchema = new Schema({
  firstname: { type: String, required: true },
  lastname: { type: String, required: true },
  email: { type: String, unique: true, required: true },
  password: { type: String, required: true },
  role: { type: Number, default: 0 },
  created_at: { type: Date, default: Date.now() },
});

UserSchema.plugin(uniqueValidator);

module.exports = model("Utilisateur", UserSchema);
